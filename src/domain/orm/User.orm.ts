import { userEntity } from "../entities/User.entity";

import {LogSuccess,LogError } from "@/utils/logger";

//Peticiones CRUD


/**
 * Metodo para obtener todos los "Users" de una coleccione de Mongo Server
 */
export const GetAllUsers = async (): Promise<any [] | undefined>  =>{

    try {
        let userModel = userEntity();

        //buscar todos los usuarios
        return await userModel.find({isDelete:false})
    } catch (error) {
        LogError(`[ORM ERROR]: GettingA All Users: ${error}`);
    }

}

//TODO
//- Get User by Id
//- Get User by Email
//- Delete User by Id
//- Create New User
//- Update User by Id