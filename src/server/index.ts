import express,{Express,Request,Response} from "express";

//variables de entorno
import dotenv from 'dotenv';

//seguridad
import cors from 'cors';
import helmet from 'helmet';

//TODO https

//*importar rutas
import rootRouter from '../routes'

//*configuracion de archivo .env
dotenv.config();

//Crear Express app
const server: Express = express();

//*Definir uso de /api y ejecutar rootRouter del 'index.ts' en routes
//A partir de aqui: http://localhost:8000/api/...
server.use(
    '/api',
    rootRouter
)

//servidor estatico
server.use(express.static('public'));

//TODO Mongoose Connection


//Conf Seguridad 
server.use(helmet());
server.use(cors());

//Contenido a mostrar
server.use(express.urlencoded({extended:true, limit:'50mb'}));
server.use(express.json({limit:'50mb'}));

//*Conf redirecciones
server.get('/',(req:Request,res:Response) => {
    res.redirect('/api');
});

export default server;