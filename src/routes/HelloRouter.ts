import { BasicResponse } from "@/controller/types";
import express, { Request,Response } from "express";
import { HelloController } from "../controller/HelloController";
import { LogInfo } from "../utils/logger";

//Router de express
let helloRouter = express.Router();

//Manera de consumir: http://localhost:8000/api/hello?name=Nombre/
helloRouter.route('/')
//GET: 
    .get(async (req:Request, res:Response) =>{
        //obtener un Query Param (Sirve para hacer consultas a traves de la URL)
        let name: any = req?.query?.name; 
        LogInfo(`Query Param: ${name}`);

        //Instancia de Controlador a metodo Execute
        const controller:HelloController = new HelloController();
        
        //obtener respuesta
        const response:BasicResponse = await controller.getMessage(name);

        //enviar respuesta al cliente
        return res.send(response);
    })

//Exportar Hello Router
export default helloRouter;