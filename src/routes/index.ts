/**
 * Root Router
 * Encargado de direccionar rutas
 */

import express,{Request,Response} from 'express';
import helloRouter from './HelloRouter';
import { LogInfo} from '../utils/logger';

//Instancia del servidor
let server = express();

//Instancia Router
let rootRouter = express.Router();

//Activar para request a http://localhost:8000/api

//GET: //Activar para request a http://localhost:8000/api/
rootRouter.get('/', (req: Request, res: Response) => {
    LogInfo('GET: http://localhost:8000/api/')
    //enviar saludo
    res.send('WELCOME to APPI RESTFULL Express + Nodemon + TS + Swagger + Mongoose');
})

//Redireccion a Rutas & Controllers
server.use('/',rootRouter); // http://localhost:8000/api/
server.use('/hello',helloRouter); // http://localhost:8000/api/hello --> HelloRouter

//Agregar mas rutas a la app...

export default server;