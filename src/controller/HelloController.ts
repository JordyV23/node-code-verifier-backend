import { Get, Query, Route, Tags } from "tsoa";
import { BasicResponse } from "./types";
import { IHelloController } from "./interfaces";
import { LogSuccess } from "../utils/logger";

@Route("/api/hello")
@Tags("HelloController")
export class HelloController implements IHelloController {

    /**
     * Endpoint para retornar el mensaje "Hello {name} como JSON"
     * @param {string | undefined} name Nombre de usuario para ser saludado
     * @returns {BasicResponse} Promesa de Basicreponse
     */
    @Get("/")
    public async getMessage(@Query()name?: string): Promise<BasicResponse> {
        LogSuccess('[/api/hello] Get Request');

        return {
            message: `Hello, ${name || "Mundo"}`
        }
    }

}