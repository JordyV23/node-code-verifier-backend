//devuelve los tipos que puede tener el controlador 

/** 
 * Basic JSON response para Controladores
 */
export type BasicResponse = {
    message: string
}

/** 
 * Error response para Controladores
 */
export type ErrorResponse = {
    error: string,
    message: string
}